require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
	def test_save_without_title
    article = Article.new(:body => 'new_body')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end 
	
	def test_save_without_body
	  article = Article.new(:title => 'new_title')
		assert_equal article.valid?, false
    assert_equal article.save, false
	end
	
end
