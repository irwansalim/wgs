require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  #test "should get new" do
  #  get :new
  #  assert_response :success
  #end
	
	def setup
    @user = User.find(:first)
  end
	
	def test_new_user
    get :new
    assert_not_nil assigns(:user)
    assert_response :success
  end
	
	def test_create_user
		assert_difference('User.count') do
	    #user = User.new(:email => 'new_user', :password => 'new_password')
      post :create, :user => {:email => 'new_user', :password => "new_password"}
      assert_not_nil assigns(:user)
            assert_equal assigns(:user).email, "new_user"
            assert_equal assigns(:user).valid?, true
    end
		assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Signed up!'
	end
	
	def test_create_user_with_invalid_parameter
    assert_no_difference('User.count') do
      post :create, :user => {:email => nil, :password => nil}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).valid?, false
    end
    assert_response :success
		assert_equal flash[:error], 'User was failed to create.'
  end
	
end
