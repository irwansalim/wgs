require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  #test "should get new" do
  #  get :new
  #  assert_response :success
  #end

  #test "should get edit" do
  #  get :edit
  #  assert_response :success
  #end

  #test "should get index" do
  #  get :index
  #  assert_response :success
  #end
	
	def setup
    @article = Article.find(:first)
  end
	
	def test_index_articles
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end
	
	def test_new_article
	  login_as('david@mail.com')
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end
	
	def test_show_article
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

	def test_edit_article
	  login_as('david@mail.com')
    get :edit, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_update_article
	  login_as('david@mail.com')
    put :update, :id => Article.first.id,
                 :article => {:title => 'updated title', :body => 'updated body'}
    assert_not_nil assigns(:article)
          assert_equal assigns(:article).title, 'updated title'
           assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Article was successfully updated.'
  end
	
	def test_update_article_with_invalid_parameter
	  login_as('david@mail.com')
    put :update, :id => Article.first.id,
                 :article => {:title => nil, :body => nil}
    assert_not_nil assigns(:article)
    assert_response :success
        assert_equal flash[:error], 'Article was failed to update.'
  end
	
	def test_create_article
		login_as('david@mail.com')		
		assert_difference('Article.count') do
      post :create, :article => {:title => 'new title', :body => "new body"}
      assert_not_nil assigns(:article)
            assert_equal assigns(:article).title, "new title"
            assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Article was successfully created.'
	end
	
	def test_create_article_with_invalid_parameter
	  login_as('david@mail.com')
    assert_no_difference('Article.count') do
      post :create, :article => {:title => nil, :body => nil}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).valid?, false
    end
    assert_response :success
        assert_equal flash[:error], 'Article was failed to create.'
  end
	
	def test_destroy_article
	  login_as('david@mail.com')
    assert_difference('Article.count', -1) do
      delete :destroy, :id => Article.first.id
      assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to articles_path
    #    assert_equal flash[:error], 'Article successfully deleted'
  end

end
