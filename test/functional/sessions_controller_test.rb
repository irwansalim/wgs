require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  #test "should get new" do
  #  get :new
  #  assert_response :success
  #end
	
	def test_create_user_session
	 	#assert_difference('User.count') do
      #post :create, :session => {:email => 'david@mail.com'}
      #assert_not_nil assigns(:session)
            #assert_equal assigns(:user).email, "new_user"
            #assert_equal assigns(:user).valid?, true
    #end
		login_as(users(:david).email)
		post :create, :email => 'david@mail.com'
		assert_not_nil session[:user_id]
		assert_equal session[:user_id], users(:david).id
		assert_response :redirect
    #assert_equal flash[:notice], 'Logged in!'
		assert_redirected_to articles_path
        
	end
	
	def test_destroy_user_session
	  login_as('david@mail.com')
		delete :destroy, :email => 'david@mail.com'
		#@request.session[:user_id] = nil
    assert_equal session[:user_id], nil
		assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Logged out!'
  end

end
