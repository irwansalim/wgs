# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

countries = Country.create([ {:code => "USA", :name => "America"},
                          {:code => "IDN", :name => "Indonesia"},
													{:code => "AUS", :name => "Australia"},
													{:code => "JPN", :name => "Japan"},
                          {:code => "ENG", :name => "England"} ]) 
													
articles = Article.create([ {:title => "Article 1", :body => "Lipsum lipsum 1"},
                          {:title => "Article 2", :body => "Lipsum lipsum 2"},
													{:title => "Article 3", :body => "Lipsum lipsum 3"},
													{:title => "Article 4", :body => "Lipsum lipsum 4"},
                          {:title => "Article 5", :body => "Lipsum lipsum 5"} ]) 
													
comments = Comment.create([ {:content => "Comment 1"},
                          {:content => "Comment 2"},
													{:content => "Comment 3"},
													{:content => "Comment 4"},
                          {:content => "Comment 5"}]) 
												
users = User.create([ {:first_name => "One", :last_name => "Eno", :email => "one@mail.com", :username => "", :address => "Lipsum A", :age => "20", :birthday => "1 October"},
                          {:first_name => "Two", :last_name => "Owt", :email => "two@mail.com", :username => "", :address => "Lipsum B", :age => "21", :birthday => "2 October"},
													{:first_name => "Three", :last_name => "Eerht", :email => "three@mail.com", :username => "", :address => "Lipsum C", :age => "22", :birthday => "4 October"},
													{:first_name => "Four", :last_name => "Rouf", :email => "four@mail.com", :username => "", :address => "Lipsum D", :age => "23", :birthday => "12 October"},
                          {:first_name => "Five", :last_name => "Evif", :email => "five@mail.com", :username => "", :address => "Lipsum E", :age => "24", :birthday => "30 October"} ]) 