class AddFokeyToTable < ActiveRecord::Migration
  def change
		add_column :articles, :user_id, :integer
		add_column :comments, :article_id, :integer
		add_column :products, :user_id, :integer
  end
end
