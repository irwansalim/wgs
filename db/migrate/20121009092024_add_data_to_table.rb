class AddDataToTable < ActiveRecord::Migration
  def change
		add_column :users, :address, :text
		add_column :users, :age, :integer
		add_column :users, :birthday, :string
		remove_column :users, :bio_profile
		change_column :users, :age, :integer
		rename_column :users, :user_name, :username
		rename_column :comments, :body, :content
  end
end
