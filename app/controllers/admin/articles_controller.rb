class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login
	protect_from_forgery
	
	#def new
  #  @article = Article.new
  #end
	
	def index
    @articles = Article.all
  end
	
	#def create
  #  @article = Articles.new
  #  redirect_to :controller => :users, :action => :index
  #end
end
