class ArticlesController < ApplicationController
  before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
	
	def new
    @article = Article.new
  end
	
	def show
	  @article = Article.find_by_id(params[:id])
		@comments = @article.comments
		@comment = Comment.new
	end

  def edit
    @article = Article.find_by_id(params[:id])
  end
        
  def update
	 @article = Article.find_by_id(params[:id])
		if @article.update_attributes(params[:article])
			flash[:notice] = 'Article was successfully updated.'
			redirect_to articles_path
		else
			flash[:error] = 'Article was failed to update.'
			render :action => "update"
		end
	end

  def index
    @articles = Article.all
  end
        
  def create
    @article = Article.new(params[:article])
     if @article.save
      flash[:notice] = 'Article was successfully created.'
      #redirect_to :action => :index
      redirect_to articles_path
    else
      flash[:error] = 'Article was failed to create.'
      render :action => "new"
      #redirect_to new_article_path
    end
  end
        
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to :controller => :articles, :action => :index
		#flash[:notice] = 'Article was successfully updated.'
  end
end