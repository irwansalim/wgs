class CommentsController < ApplicationController

  def create
	  @comment = Comment.new(params[:comment])
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(article_path(article), :notice => 'Comment was successfully created.') }
        format.js { @comments = Article.find(params[:comment][:article_id].to_i).comments }
      end
    end
  end
end
