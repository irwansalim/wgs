class UsersController < ApplicationController
	before_filter :already_login, :only => [:new, :create, :edit, :update, :delete]
  def new
		@user = User.new
  end
	
	def create
	  @user = User.new(params[:user])
		if @user.save
		  redirect_to articles_path, :notice => "Signed up!"
		else
		  flash[:error] = 'User was failed to create.'
      render :action => "new"
		end
	end
	
	#def destroy
  #  session[:user_id] = nil
  #  redirect_to articles_path, :notice => "Logged out!"
  #end 
	
end
