class Product < ActiveRecord::Base
  #attr_accessible :name, :price, :description
	belongs_to :user
	has_many :categories, :through => :products_categories #join table
  has_many :products_categories
	default_scope where("price > 1000")
	default_scope where("name LIKE 'red'")
end
