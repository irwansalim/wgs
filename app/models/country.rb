class Country < ActiveRecord::Base
  #attr_accessible :code, :name
	
	validates_inclusion_of :code,  :in => %w( id usa frc ), :message => "You need choose between ID / USA / FRC"
end
