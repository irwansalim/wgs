class User < ActiveRecord::Base
  #attr_accessible :first_name, :last_name, :email, :username, :address, :age, :birthday, :password, :password_confirmation
	has_many :products,  :dependent => :destroy # plural
	has_many :articles,  :dependent => :destroy # plural
	
	#TEST CASE
	#validates :first_name, :presence => true
  #validates :last_name, :presence => true
	validates :email, :presence => true
  #validates :username, :presence => true
	#validates :address, :presence => true
  #validates :age, :presence => true
	#validates :birthday, :presence => true
  #validates :password_hash, :presence => true
	#validates :password_salt, :presence => true
  scope :first_name_case, where("first_name like '%share%'")
  #END OF TEST CASE
	
	def is_admin?
		self.id == 11
	end
	
	#def show_full_address
  #  "#{self.address} #{self.country}"
  #end
	
	attr_accessor :password, :password_confirmation
	before_save :encrypt_password
  #validates :password, :presence => {:on => :create},
  #                     :confirmation => true
  #validates :email, :presence => true, :uniqueness => true

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  
	def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
	end
	
	#validates :first_name,  :presence => true, 
  #                 :length => {:minimum => 1, :maximum => 20},
  #                  :format => {:with => /[a-zA-Z\s]+$/}
               
  #validates :email, :presence => true,
  #                  :length => {:minimum => 3, :maximum => 254},
  #                  :uniqueness => true
  #                  :format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
end
