class Article < ActiveRecord::Base
  #attr_accessible :title, :body
	has_many :comments,  :dependent => :destroy # plural
	belongs_to :user
	#has_many :more_content,
  #         :class_name => "User" ,
  #           :foreign_key => "user_id" ,
  #           :conditions => "status = SELECT * FROM articles WHERE title LIKE '%Lip%'"
	#default_scope where("rating = 5")
	validates :title, :presence => { :message => "Article title is required" },
                    :uniqueness => true
	#validates_length_of :title, :in => 7..32, :allow_blank => false
	validates :body, :presence => { :message => "Article content is required"}
	#def self_article_content
		#"#{self.where("LENGTH(body) > 1")}"
  #end
end
