module ApplicationHelper

def welcome_text
  str = "" # if the user has logged in, show the welcome text.
  if current_user
    str = "Welcome, #{current_user.first_name} | "
    str += link_to "Logout", log_out_path
  else
    str = "Welcome, Guest | "
		str += "#{link_to "Login", login_path} | "
    str += link_to "Signup", sign_up_path
  end
end 

def restriction_add
  str = "" # if the user has logged in, show the welcome text.
  if current_user 
    str = link_to "Add New Article", new_article_path
  else
    str = ""
  end
end 

def restriction_edit
  str = "" # if the user has logged in, show the welcome text.
  if current_user #&& session[:user_id] == @articles.user_id
    str = link_to "Edit", edit_article_path(@articles)
		str += " | "
    str += link_to "Delete", article_path(@articles), :method => :delete
  else
    str = ""
  end
end 

end
